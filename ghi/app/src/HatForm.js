import React, {useState, useEffect} from 'react'

function HatForm(props){
    const [locations, setLocations] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/locations/";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();

            setLocations(data.locations)
        }
    }
    // fabric handler
    const [fabric, setFabric] = useState('');
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    // style_name handler
    const [style, setStyle] = useState('');
    const handleStyleChange = (event) => {
        const value = event.target.value;
        setStyle(value);
    }
    // color handler
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    // img_url handler
    const [imgUrl, setImgUrl] = useState('');
    const handleImgUrlChange = (event) => {
        const value = event.target.value;
        setImgUrl(value);
    }
    // location handler
    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }
    // form submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.fabric = fabric;
        data.style_name = style;
        data.color = color;
        data.img_url = imgUrl;
        data.location = location;

        console.log(data);

        const hatUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setFabric('');
            setStyle('');
            setColor('');
            setImgUrl('');
            setLocation('');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Hat to the Collection</h1>
                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" />
                            <label htmlFor="fabric">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={style} onChange={handleStyleChange} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" />
                            <label htmlFor="style_name">Hat Style</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={imgUrl} onChange={handleImgUrlChange} placeholder="img_url" required type="url" name="img_url" id="img_url" className="form-control" />
                            <label htmlFor="img_url">Image Url</label>
                        </div>
                        <div className="mb-3">
                            <select value={location} onChange={handleLocationChange} name="location" id="location" className="form-select">
                                <option value="">Choose location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm
