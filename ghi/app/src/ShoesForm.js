import React, {useState, useEffect} from 'react'

function ShoesForm(props){
    const [bins, setBins] = useState([]);

    const fetchData = async () => {
        const url = "http://localhost:8100/api/bins/";

        const response = await fetch(url);
        if(response.ok){
            const data = await response.json();
            setBins(data.bins)
        }
    }
    // manufacturer handler
    const [manufacturer, setManufacturer] = useState('');
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    // model name handler
    const [modelName, setModelName] = useState('');
    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }
    // color handler
    const [color, setColor] = useState('');
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    // image url handler
    const [imgUrl, setImgUrl] = useState('');
    const handleImgUrlChange = (event) => {
        const value = event.target.value;
        setImgUrl(value);
    }
    // bin handler
    const [bin, setBin] = useState('');
    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }
    // form submit handler
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.img_url = imgUrl;
        data.bin = bin;

        console.log(data);

        const shoesUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            },
        };
        const response = await fetch(shoesUrl, fetchConfig);
        if (response.ok){
            const newShoes = await response.json();
            console.log(newShoes);

            setManufacturer('');
            setModelName('');
            setColor('');
            setImgUrl('');
            setBin('');
        }
    }
    useEffect(() => {
        fetchData();
    }, []);
    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add Some Shoes to the Collection</h1>

                    <form onSubmit={handleSubmit} id="create-hat-form">
                        <div className="form-floating mb-3">
                            <input value={manufacturer} onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={modelName} onChange={handleModelNameChange} placeholder="model_name" required type="text" name="model_name" id="model_name" className="form-control" />
                            <label htmlFor="model_name">Model name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={imgUrl} onChange={handleImgUrlChange}placeholder="img_url" required type="text" name="img_url" id="img_url" className="form-control" />
                            <label htmlFor="img_url">Image url</label>
                        </div>
                        <div className="mb-3">
                            <select value={bin} onChange={handleBinChange}name="bin" id="bin" className="form-select">
                                <option value="">Choose location</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>
                                            {bin.closet_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}


export default ShoesForm
