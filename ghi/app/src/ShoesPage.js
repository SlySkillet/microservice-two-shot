import React, {useState, useEffect} from 'react'
import { NavLink } from 'react-router-dom'

function ShoesPage(props) {
    const [shoes, setShoes] = useState([]);
    useEffect(() => {
        let ignore = false
        async function loadShoes(){
            const response = await fetch('http://localhost:8080/api/shoes/');
            if (response.ok){
                const data = await response.json();
                if (!ignore){
                    setShoes(data.shoes)
                }
            } else {
                console.error(response);
            }
        }
        loadShoes()
    }, []) // call shoes in [] - starts an infinite loop sending api calls - how to make this only call when there is a change to the database?
    const deleteShoes = async (id, e) => {
        const shoesDetailUrl = `http://localhost:8080/api/shoes/${id}/`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(shoesDetailUrl, fetchConfig);
        if (response.ok){
            console.log("response ==>", response)
            console.log("shoes", shoes)
            setShoes((shoes) => shoes.filter((pair) => pair.id !== id))
        }
    }

    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold">broh, these are your shoes 👀</h1>
            <button className="btn btn-primary"><NavLink className="link-light" to="/addshoes">Add some shoes</NavLink></button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model Name</th>
                        <th>Color</th>
                        <th>Image Url</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                {shoes.map(pair => {
                    return(
                        <tr key={pair.id}>
                            <td>{pair.manufacturer}</td>
                            <td>{pair.model_name}</td>
                            <td>{pair.color}</td>
                            <td><img src={pair.img_url} width={80}/></td>
                            <td>{pair.bin.closet_name}</td>
                            <td>
                            <button className="btn btn-warning" onClick={(e) => deleteShoes(pair.id, e)}>delete</button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>

        </div>
    )
}


export default ShoesPage;
