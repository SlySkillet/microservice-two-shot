import React, {useState, useEffect} from 'react'
import { NavLink } from 'react-router-dom';

function HatsPage(props) {
    const [hats, setHats] = useState([])
    useEffect( () => {
        async function loadHats() {
            const response = await fetch('http://localhost:8090/api/hats/');
            if (response.ok){
              const data = await response.json();
            //   console.log(data);
              setHats(data.hats);
            } else{
              console.error(response);
            }
          }
          loadHats();
    }, []);
    // delete hat functionality
    console.log("state:", hats)
    const deleteHat = async (id, e) => {
        console.log("id", id )
        const hatDetailUrl = `http://localhost:8090/api/hats/${id}/`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(hatDetailUrl, fetchConfig);
        if (response.ok){
            console.log(hats)
            let newData = hats.filter((hat) => hat.id !== id)
            console.log("newaData ==>", newData)
            console.log("hats 1st log ==>", hats)
            setHats(newData)
            console.log("hats 2nd log ==>", hats)
            // window.location.reload()
        }
    }


    return (
        <div className="px-4 py-5 my-5 text-center">
            <h1 className="display-5 fw-bold">👀these are your hats broh 👀</h1>
            <button className="btn btn-primary"><NavLink className="link-light" to="/addhat">Add a Hat</NavLink></button>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>Img Url</th>
                        <th>Location</th>
                    </tr>
                </thead>
                <tbody>
                {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td><img src={hat.img_url} width={80}/></td>
                            <td>{hat.location.closet_name}</td>
                            <td>
                                <button className="btn btn-warning" onClick={(e) => deleteHat(hat.id, e)}>delete</button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        </div>
    )
}

export default HatsPage;
