import { BrowserRouter, Routes, Route } from 'react-router-dom';
import HatForm from './HatForm';
import MainPage from './MainPage';
import HatsPage from './HatsPage';
import Nav from './Nav';
import ShoesPage from './ShoesPage';
import ShoesForm from './ShoesForm';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/addshoes" element={<ShoesForm />} />
          <Route path="/shoes" element={<ShoesPage />} />
          <Route path="/hats" element={<HatsPage hats={props} />} />
          <Route path="/addhat" element={<HatForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
