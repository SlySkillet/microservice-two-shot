from django.urls import path

from .views import list_hats, detail_hat

urlpatterns = [
    path("hats/<int:pk>/", detail_hat, name="detail_hat"),
    path("hats/", list_hats, name="list_hats"),
]
