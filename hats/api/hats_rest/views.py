from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "img_url",
        "location",
        "id"
    ]
    encoders = {
        "location": LocationVODetailEncoder()
    }


class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "img_url",
        "id",
        "location",
    ]
    encoders ={
        "location": LocationVODetailEncoder()
    }


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method =="GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location=location_vo_id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
        { "hats": hats},
        encoder= HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        location_href = content["location"]
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE"])
def detail_hat(request, pk):
    if request.method =="GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
